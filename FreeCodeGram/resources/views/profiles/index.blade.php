@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="https://instagram.fgye7-1.fna.fbcdn.net/v/t51.2885-19/s150x150/84552864_1058917204442277_8779656581798166528_n.jpg?_nc_ht=instagram.fgye7-1.fna.fbcdn.net&_nc_ohc=wp0PSUbYQ0wAX_tBYf_&oh=1357fe72ecac96eabcfa1e48f921e410&oe=5F4FB795" class="rounded-circle" alt="">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>{{$user->username}}</h1>
                <a href="/p/create" class=pt>Add New Post</a>
            </div>
            <div class="d-flex">
                <div class="pr-3"><p><strong>{{$user->posts->count()}}</strong> publicaciones</p></div>
                <div class="pr-3"><p><strong>486</strong> seguidores</p></div>
                <div><p><strong>3.978</strong> seguidos</p></div>
            </div>
            <div class="pt-4 font-weight-bold"><strong>{{ $user->profile->title  }}</strong></div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="#">{{ $user->profile->url }}</a> </div>
        </div>
    </div>

    <div class="row">
        @foreach($user->posts as $post)
        <div class="col-4 pt-5 pb-4">
            <img src="/storage/{{$post->image}}" class="w-100">
        </div>
        @endforeach
    </div>
</div>
@endsection
